package com.codehub.projectfuture.webapp.model;

import com.codehub.projectfuture.webapp.domain.Owner;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class RepairModel {
    private Long id;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    private String status;
    private String fixType;
    private Double cost;
    private String address;
    private String ownerTaxRegistryNumber;
    private String description;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getId() {
       return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFixType() {
        return fixType;
    }

    public void setFixType(String fixType) {
        this.fixType = fixType;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getAddress() {return address;}

    public void setAddress(String address) {this.address=address;}

    public String getOwnerTaxRegistryNumber() {
        return ownerTaxRegistryNumber;
    }

    public void setOwnerTaxRegistryNumber(String ownerTaxRegistryNumber) {
        this.ownerTaxRegistryNumber = ownerTaxRegistryNumber;
    }

    public String getDescription() {return description;}

    public void setDescription(String description) {this.description = description;}

    public RepairModel() {
    }

    public RepairModel(Long id, String status, String ownerTaxRegistryNumber, LocalDate date, String fixType, Double cost, String description, String address) {
        this.id=id;
        this.date = date;
        this.status =status;
        this.fixType = fixType;
        this.cost = cost;
        this.address=address;
        this.ownerTaxRegistryNumber = ownerTaxRegistryNumber;
        this.description = description;


    }

}