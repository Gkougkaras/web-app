package com.codehub.projectfuture.webapp.enums;

public enum FixType {
    PAINT("Paint"),
    INSULATION("Insulation"),
    HOLLOW("Hollow"),
    HYDRAULIC_REPAIRS("Hydraulic Repairs"),
    ELECTRICAL_REPAIRS("Electrical Repairs");

    private String fullName;

    FixType(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
}
