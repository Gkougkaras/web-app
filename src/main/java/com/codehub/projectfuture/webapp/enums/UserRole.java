package com.codehub.projectfuture.webapp.enums;

public enum UserRole {
    ADMIN("Admin"),
    USER("User");

    private String fullName;

    UserRole(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
}
