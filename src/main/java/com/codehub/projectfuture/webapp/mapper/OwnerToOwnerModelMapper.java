package com.codehub.projectfuture.webapp.mapper;

import com.codehub.projectfuture.webapp.domain.Owner;
import com.codehub.projectfuture.webapp.enums.PropertyType;
import com.codehub.projectfuture.webapp.model.OwnerModel;
import org.springframework.stereotype.Component;

@Component
public class OwnerToOwnerModelMapper {

    public OwnerModel mapToOwnerModel(Owner owner) {
        OwnerModel ownerModel = new OwnerModel();
        ownerModel.setTaxRegistryNumber(owner.getTaxRegistryNumber().toString());
        ownerModel.setFirstName(owner.getFirstName());
        ownerModel.setLastName(owner.getLastName());
        ownerModel.setAddress(owner.getAddress());
        ownerModel.setEmail(owner.getEmail());
        ownerModel.setPhoneNumber(owner.getPhoneNumber());
        ownerModel.setType(owner.getType().getFullName());
        ownerModel.setRole(owner.getRole().getFullName());
        return ownerModel;
    }
}

