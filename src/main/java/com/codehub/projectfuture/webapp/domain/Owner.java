package com.codehub.projectfuture.webapp.domain;

import com.codehub.projectfuture.webapp.enums.PropertyType;
import com.codehub.projectfuture.webapp.enums.UserRole;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "OWNER", uniqueConstraints = {@UniqueConstraint(columnNames = {"firstname", "lastname"})})
public class Owner {

    private static final int MAX_NAME_LENGTH=60;
    private static final int MAX_PASSWORD_LENGTH=100;
    private static final int MAX_SSN_LENGTH=9;

    @Id
    @Column(name = "taxRegistryNumber", nullable = false, length = MAX_SSN_LENGTH)
    private Long taxRegistryNumber;

    @Column(name = "firstname", length = MAX_NAME_LENGTH)
    private String firstName;

    @Column(name = "lastname", length = MAX_NAME_LENGTH)
    private String lastName;

    @Column(name = "address")
    private String address;

    @Column(name = "phonenumber")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "password", length = MAX_PASSWORD_LENGTH)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "property_type")
    private PropertyType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private UserRole role;

    @OneToMany(mappedBy = "owner", targetEntity = com.codehub.projectfuture.webapp.domain.Repair.class, cascade= {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<com.codehub.projectfuture.webapp.domain.Repair> repairs;


    public Owner(Long taxRegistryNumber, String firstName, String lastName, String address, String phoneNumber, String email, String password, PropertyType type, List<Repair> repairs, UserRole role) {
        this.taxRegistryNumber = taxRegistryNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.type = type;
        this.repairs = repairs;
        this.role = role;
    }

    public Owner() {

    }

    public Owner(Long ownerTaxRegistryNumber) {
    }

    public Long getTaxRegistryNumber() {
        return taxRegistryNumber;
    }

    public void setTaxRegistryNumber(Long taxRegistryNumber) {
        this.taxRegistryNumber = taxRegistryNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PropertyType getType() {
        return type;
    }

    public void setType(PropertyType type) {
        this.type = type;
    }

    public List<com.codehub.projectfuture.webapp.domain.Repair> getRepairs() {
        return repairs;
    }

    public void setRepairs(List<com.codehub.projectfuture.webapp.domain.Repair> repairs) {
        this.repairs = repairs;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        final StringBuilder sb=new StringBuilder("Owner {");
        sb.append("taxRegistryNumber=").append(taxRegistryNumber);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", phoneNumber='").append(phoneNumber).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", propertyType='").append(type).append('\'');
        sb.append(", userRole='").append(role).append('\'');
        sb.append('}');
        return sb.toString();


    }
}

