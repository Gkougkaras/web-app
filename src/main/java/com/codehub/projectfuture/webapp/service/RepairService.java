package com.codehub.projectfuture.webapp.service;

import com.codehub.projectfuture.webapp.domain.Repair;
import com.codehub.projectfuture.webapp.model.RepairModel;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RepairService {

    List<RepairModel> findAll();

    Repair createRepair(Repair repair);

    Repair updateRepair(RepairModel repairModel);

    void deleteById(Long id);

    Optional<RepairModel> findRepair(Long id);

    List<RepairModel> findByDate(LocalDate date);

    List<RepairModel> findByOwnerTaxRegistryNumber(Long taxRegistryNumber);

    List<RepairModel>findByDateBetween(LocalDate date,LocalDate date1);

    List<RepairModel> findByOwner(String name);
}
