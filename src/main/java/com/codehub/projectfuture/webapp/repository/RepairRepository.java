package com.codehub.projectfuture.webapp.repository;

import com.codehub.projectfuture.webapp.domain.Repair;
import com.codehub.projectfuture.webapp.model.RepairModel;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.print.DocFlavor;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RepairRepository extends JpaRepository<Repair, Long> {

    Repair save(Repair repair);

    void deleteById(Long id);

    List<Repair> findAll();

    List<Repair> findByDate(LocalDate date);

    List<Repair> findByOwnerTaxRegistryNumber(Long taxRegistryNumber);

    Optional<Repair> findById(Long id);

    List<Repair> findByDateBetween(LocalDate date,LocalDate date1);
}

