package com.codehub.projectfuture.webapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AboutUsController {


    @GetMapping("/about_us")
    private String aboutUs() {
        return "aboutUs";
    }

}
